def isPalindrome(s):
    return s == s[::-1]

def fib(n):
    if n <= 1:
        return n

    if n > 0:
        return fib(n - 1) + fib(n - 2)

s = "malayalam"
ans = isPalindrome(s)

if ans:
    print("Yes")
else:
    print("No")

n = input("Enter your value: ")
print(n)